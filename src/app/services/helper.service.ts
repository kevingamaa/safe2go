import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HelperService {
    public cssClasses: any = '';
    public isLoading: boolean;
    constructor(public toastCtrl: ToastController) { }


    public loading() {
        setTimeout(() => this.isLoading = true, 100);
        setTimeout(this.stopLoading, 5000);
    }

    public stopLoading() {
        this.isLoading = false;
    }

    async toast(message = { title: '', text: '' }, options: any) {
        const optionsToSend: any = typeof options === 'string' ? {} : options;
        if (!optionsToSend.buttons) {
            optionsToSend.buttons = [
                {
                    text: 'Ok',
                    role: 'cancel'
                }
            ];
        }
        if (typeof options === 'string' || !optionsToSend.color) {
            optionsToSend.color = options ? options : 'primary';
            // console.log( optionsToSend.color);
        }

        if (!optionsToSend.position) {
            optionsToSend.position = 'top';
        }
        optionsToSend.header = message.title;
        optionsToSend.message = message.text;
        optionsToSend.duration = 5000;
        const toast = await this.toastCtrl.create(optionsToSend);
        toast.present();
    }
}
