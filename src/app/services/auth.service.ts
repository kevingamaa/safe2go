import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { HelperService } from './helper.service';
import { Model } from '../model';
import { HttpClient } from '@angular/common/http';

export class Credentials {
    public usernam: string;
    public password: string;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService extends Model {
    public authState = new BehaviorSubject(false);
    public user: any;
    public key: any;
    protected apiUrl = 'auth';
    constructor(
        private router: Router,
        private storage: Storage,
        private platform: Platform,
        public helper: HelperService,
        protected http: HttpClient
    ) {
        super(http, helper);
        this.platform.ready().then(() => {
            this.ifLoggedIn();
        });
    }

    public ifLoggedIn() {
        // console.log(this.user)
        this.storage.get('user').then(userInfo => {
            this.setUser(userInfo);
        });
    }


    public login(credencials: Credentials) {
        this.helper.loading();
        return this.http.post(`${this.url}/login`, credencials).subscribe((response: any) => {
            this.storage.set('user', response).then(user => this.setUser(user));
            // console.log()
            this.router.navigate(['/dash']);
            this.helper.stopLoading();
        }, error => this.handleError(error));
    }

    public logout() {
        this.storage.remove('user').then(() => {
            this.router.navigate(['/auth/login']);
            this.authState.next(false);
        });
    }

    public setUser(userInfo: any) {
        if (userInfo) {
            this.user = userInfo.user;
            this.key = userInfo['x-access-token'];
            this.authState.next(true);
        }
    }

    public isAuthenticated() {
        return this.authState.value;
    }


}



