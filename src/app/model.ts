import { Observable, throwError } from 'rxjs';
import { retry, catchError, finalize } from 'rxjs/operators';
import { HelperService } from './services/helper.service';
import { HttpClient } from '@angular/common/http';
export class Model {

    protected endpoint = 'http://beta-api.w1consultoria.com.br';
    protected apiUrl   = '';

    constructor(protected http: HttpClient, public helper: HelperService) {

    }
    public get url() {
        return `${this.endpoint}/${this.apiUrl}`;
    }

    public get(parans: any): Observable<any> {
        this.helper.loading();
        return this.http.get<any>(`${this.url}/${this.parans(parans)}`).pipe(
           finalize(() => {
               this.helper.stopLoading();
           }),
           catchError(this.handleError)
        );
    }

    public find(id: number) {
        this.helper.loading();
        return this.http.get<any>(`${this.url}/${id}`).pipe(
            finalize(() => {
                this.helper.stopLoading();
            }),
            catchError(this.handleError)
        );
    }

    public create(id: number, data: any): Observable<any> {
        this.helper.loading();
        return this.http.post<any>(`${this.url}/${id}`, data).pipe(
            finalize(() => {
                this.helper.stopLoading();
            }),
            catchError(this.handleError)
        );
    }

    public update(id: number, data: any): Observable<any> {
        this.helper.loading();
        return this.http.put<any>(`${this.url}/${id}`, data).pipe(
            finalize(() => {
                this.helper.stopLoading();
            }),
            catchError(this.handleError)
        );
    }

    public delete(id: number): Observable<any> {
        this.helper.loading();
        return this.http.delete<any>(`${this.url}/${id}`).pipe(
            finalize(() => {
                this.helper.stopLoading();
            }),
            catchError(this.handleError)
        );
    }

    public parans(parans) {
        let urlParans = '';
        for (const i in parans) {
            if (i) {
                urlParans += `${i}=${JSON.stringify(parans[i])}`;
            }
        }
        return urlParans;
    }


    public handleError(error) {
        let errorMessage = '';
        if (typeof error.error !== 'object') {
            if (error.error instanceof ErrorEvent) {
                // client-side error
                errorMessage = `Error: ${error.error.message}`;
            } else {
                // server-side error
                errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
            this.helper.toast({
                title: 'Aviso',
                text: errorMessage
            }, 'danger');
        } else {
            for (const i in error.error) {
                if (i) {
                    this.helper.toast({
                        title: 'Aviso',
                        text: error.error[i]
                    }, 'danger');
                }
            }
        }

        this.helper.stopLoading();
        return throwError(error);
    }
}


