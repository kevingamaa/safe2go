import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    group,
    // ...
} from '@angular/animations';
import { AuthService } from 'src/app/services/auth.service';
import { RouterEvent, RouterLinkActive } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: [
        trigger('register', [
            transition(':enter', [
                style({
                    opacity: '0',
                    transform: 'translate(300px, 0)'
                }),
                group([
                    animate('500ms ease-out', style({
                        opacity: '1',
                        transform: 'translate(0px, 0px)'
                    }))
                ])
            ])
        ])

    ]
})
export class RegisterComponent implements OnInit, OnDestroy {
    public form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        cpf: ['', Validators.required ],
        password: ['', Validators.required],
        confirm_password: ['', Validators.required]
    });

    constructor(
        private fb: FormBuilder,
        public auth: AuthService
    ) { }

    ngOnInit() {
        setTimeout(() => this.auth.helper.cssClasses = true, 100);
    }

    ngOnDestroy(): void {
        this.auth.helper.cssClasses = false;
    }
}
