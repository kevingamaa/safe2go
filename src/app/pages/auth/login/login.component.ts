import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    group,
    // ...
} from '@angular/animations';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [
        trigger('login', [
            transition(':enter', [
                style({
                    opacity: '0',
                    transform: 'translate(-300px, 0)'
                }),
                group([
                    animate('500ms ease-out', style({
                        opacity: '1',
                        transform: 'translate(0px, 0px)'
                    }))
                ])
            ])
        ])

    ]
})
export class LoginComponent implements OnInit, OnDestroy {
    public form: FormGroup = this.fb.group({
        username: ['', [Validators.required]],
        password: ['', Validators.required]
    });

    public showPassword = true;

    constructor(
        private fb: FormBuilder,
        private auth: AuthService
    ) { }

    ngOnInit() {

    }

    public togglePassword() {
        this.showPassword = this.showPassword ? false : true;
    }

    public authenticate(): any {
        // console.log('tá aqui');
        this.auth.login(this.form.value);
    }


    ngOnDestroy() {
        if (!this.auth.authState) {
            this.auth.login(this.form.value).unsubscribe();
        }
    }
}

