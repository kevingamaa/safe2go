import { Component, OnInit, ViewChild } from '@angular/core';
import listPlugin from '@fullcalendar/list';
import ptBrLocale from '@fullcalendar/core/locales/pt-br';
import { FullCalendarComponent } from '@fullcalendar/angular';

@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {
    public calendarPlugins = [listPlugin];
    public calendarEvents = [
        { title: 'event 1', date: '2019-04-01' }
    ];
    public locale: any = ptBrLocale;
    constructor() { }

    ngOnInit() {
       this.addEvent();
    }

    addEvent() {
        this.calendarEvents = this.calendarEvents.concat([ // creates a new array!
            { title: 'event 2', date: '2019-08-19' }
        ]);
    }

    modifyTitle(eventIndex, newTitle) {
        const calendarEvents = this.calendarEvents.slice(); // a clone
        const singleEvent = Object.assign({}, calendarEvents[eventIndex]); // a clone
        singleEvent.title = newTitle;
        calendarEvents[eventIndex] = singleEvent;
        this.calendarEvents = calendarEvents; // reassign the array
    }
}
